package szasz_zsiga_nikolett_lab2_ex2;
import java.util.Scanner;
public class PrintNumberInWord {
    public static void ex2switch(int number)
    {
        String no;
        switch (number) {
            case 1: no="ONE";
                break;
            case 2: no="TWO";
                break;
            case 3: no="THREE";
                break;
            case 4: no="FOUR";
                break;
            case 5: no="FIVE";
                break;
            case 6: no="SIX";
                break;
            case 7: no="SEVEN";
                break;
            case 8: no="EIGHT";
                break;
            case 9: no="NINE";
                break;
            default: no="OTHER";
        }
        System.out.println(no);
    }
    public static void ex2ifelse(int number)
    {   String no;
        if(number>9) no="OTHER";
        else if(number==1) no="ONE";
        else if (number==2) no="TWO";
        else if(number==3) no="THREE";
        else if(number==4) no="FOUR";
        else if(number==5) no="FIVE";
        else if (number==6) no="SIX";
        else if(number==7) no="SEVEN";
        else if(number==8) no="EIGHT";
        else no="NINE";
        System.out.println(no);
    }
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        int number = in.nextInt();
        System.out.println("Switch:\n");
        ex2switch(number);
        System.out.println("\n If else:\n");
        ex2ifelse(number);
        }
    }

