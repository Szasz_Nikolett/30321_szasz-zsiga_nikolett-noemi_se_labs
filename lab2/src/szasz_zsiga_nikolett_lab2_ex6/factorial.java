package szasz_zsiga_nikolett_lab2_ex6;

import java.util.Scanner;

public class factorial {

    public static int recursive(int i) {
        if (i >= 1) return i * recursive(i - 1);
        else return 1;
    }

    public static int nonrecursive(int i) {
        int prod = 1;
        for (int x = 1; x <= i; x++)
            prod = prod * x;
        return prod;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int number = in.nextInt();
        int rec = recursive(number);
        int non_rec = nonrecursive(number);
        System.out.print("Recursive: " + rec + "\n");
        System.out.print("Non-Recursive: " + non_rec + "\n");
    }
}