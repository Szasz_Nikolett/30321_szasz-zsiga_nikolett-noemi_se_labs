package szasz_zsiga_nikolett_lab2_ex7;
import java.lang.Math;

import java.util.Scanner;
public class Guess {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        double a = Math.random();
        double read;
        boolean cond = false;
        for (int i = 1; i <= 3; i++) {
            read = in.nextDouble();
            if (read == a) {
                System.out.print("Match");
                cond = true;
            } else if (read > a) System.out.print("Wrong answer, your number it too high");
            else System.out.print("Wrong answer, your number it too low");
        }
        if (cond == false) System.out.print("you lost");
    }
}
