package szasz_zsiga_nikolett_lab2_ex4;

import java.util.Scanner;
public class MaxArray {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("N=");
        int n = in.nextInt();
        int[] arr = new int[n];
        System.out.print("Read the elmenets:");
        for (int i = 0; i < n; i++)
            arr[i] = in.nextInt();
        int maxim = arr[0];
        for (int i = 1; i < n; i++)
            if (arr[i] > maxim) maxim = arr[i];
        System.out.print("Maximum: " + maxim);
    }
}
