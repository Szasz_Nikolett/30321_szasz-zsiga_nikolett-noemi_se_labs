package package_entities;

import package_actions.LunchBreak;
import package_actions.WorkDay;

import java.net.PortUnreachableException;

public class Employee extends Person implements WorkDay, LunchBreak {
    private boolean working;
    private int departmentId;

    public Employee(int id, String name,String address, boolean working,int departmentId)
    {
        this.id=id;
        this.name=name;
        this.address=address;
        this.working=working;
        this.departmentId=departmentId;
    }
    public void work(){
        System.out.println("This Employee works ");
    }

    public void relax()
    {
        System.out.println("This employee relaxes ");
    }

    public int getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(int departmentId) {
        this.departmentId = departmentId;
    }

    public boolean isWorking() {
        return working;
    }

    public void setWorking(boolean working) {
        this.working = working;
    }

    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name=name;
    }
    public String getAddress()
    {
        return address;
    }
    public  void setAddress(String address)
    {
        this.address=address;
    }
    public int getId()
    {
        return id;
    }
    public void setId(int id)
    {
        this.id=id;
    }
    public boolean equals(Object obj)
    {
        if(obj instanceof Employee){
            Employee p = (Employee)obj;
            return id == p.id;
        }
        return false;

    }
    public String toString()
    {
        return "ID: "+id+" Name: "+name+" Address: "+address+" Department id: "+departmentId+" Working: "+working;
    }

}
