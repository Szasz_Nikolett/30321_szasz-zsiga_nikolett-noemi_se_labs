package package_entities;

import package_departments.Department;
import package_departments.Finance;
import package_departments.IT;

import java.util.ArrayList;

public class Company {
    String name;
    String address;
    private ArrayList<Department> departments=new ArrayList<Department>();
    public Company(String name, String address)
    {
        this.name=name;
        this.address=address;
    }

    public void intDepartments()
    {
        Finance finance=new Finance();
        IT it=new IT();
        departments.add(finance);
        departments.add(it);
        Department department=null;
        for(int i=0;i<departments.size();i++) {
            department = (Department) departments.get(i);
            System.out.println(department.toString());
        }

    }



}
