package package_entities;

public class Programmer extends Employee{

    public Programmer(int id, String name, String address, boolean working, int departmentId)
    {
        super(id,name,address,working,departmentId);
    }
    @Override
    public void work() {
        super.work();
        System.out.println(" Programmer name: "+this.name);
    }

    @Override
    public void relax() {
        super.relax();
        System.out.println(" Programmer name: "+this.name);
    }

}
