package package_entities;

public class Accountant extends Employee{

    public Accountant(int id, String name, String address, boolean working, int departmentId)
    {
        super(id,name,address,working,departmentId);
    }
    @Override
    public void work() {
        super.work();
        System.out.println(" Accountant name: "+this.name);
    }

    @Override
    public void relax() {
        super.relax();
        System.out.println(" Accountant name: "+this.name);
    }
}
