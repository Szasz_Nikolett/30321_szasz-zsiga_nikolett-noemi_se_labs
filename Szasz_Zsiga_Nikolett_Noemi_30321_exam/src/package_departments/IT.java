package package_departments;

import package_entities.Accountant;
import package_entities.Employee;
import package_entities.Programmer;

import java.util.ArrayList;
import java.util.Scanner;

public class IT extends Department{

    private ArrayList<Programmer> programmers=new ArrayList<Programmer>();
    public IT()
    {
        super(2,"IT");
    }

    @Override
    public void addEmployee(Employee e) {
        Programmer newEmployee=(Programmer) e;
        programmers.add(newEmployee);
    }

    @Override
    public void removeEmployee(Employee e) {
        Programmer newEmployee=(Programmer) e;
        Programmer programmer=null;
        boolean exist=false;
        for(int i=0;i<programmers.size();i++)
        {   programmer=programmers.get(i);
            if(newEmployee.equals(programmer))
            {
                exist=true;
                programmers.remove(newEmployee);
                break;
            }
        }


        if(exist==false) {
            System.out.println("The employee doesn't exist");
        }
    }



    @Override
    public Employee getEmployee(int id) {
        int i=0;
        Programmer programmer=null;
        for(;i<programmers.size();i++)
        {
            programmer=(Programmer) programmers.get(i);
            if(programmer.getId()==id)
                break;
        }
        if(programmer==null)
        {

            System.out.println("No such employee");
        }
        return programmer;
    }


    @Override
    public void updateEmployee(Employee e) {
        Programmer updatedEmployee=(Programmer) e;
        int id,departmentId;
        String name, address;
        boolean working;
        boolean exist=false;
        Scanner in=new Scanner(System.in);
        Programmer programmer=null;
        for(int i=0;i<programmers.size();i++) {
            programmer = programmers.get(i);
            if (updatedEmployee.equals(programmer)) {
                exist = true;
                System.out.println("ID: ");
                id = in.nextInt();
                programmer.setId(id);
                System.out.println("Name: ");
                name = in.nextLine();
                programmer.setName(name);
                System.out.println("Address: ");
                address = in.nextLine();
                programmer.setAddress(address);
                System.out.println("WorkingStatus: ");
                working = in.nextBoolean();
                programmer.setWorking(working);
                programmers.set(i, programmer);
                break;
            }
        }
        if(!exist)
        {
            System.out.println("The employee doesn't exist");
        }

    }

    public void printAll()
    {
        Programmer programmer=null;
        for(int i=0;i< programmers.size();i++)
        {
            programmer=(Programmer) programmers.get(i);
            System.out.println(programmer.toString());
        }
    }
}
