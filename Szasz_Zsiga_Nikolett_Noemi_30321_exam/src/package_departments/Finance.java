package package_departments;

import package_entities.Accountant;
import package_entities.Employee;
import package_entities.Programmer;


import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Scanner;

public class Finance extends Department{
    private ArrayList<Accountant> accountants=new ArrayList<Accountant>();

   public Finance()
    {
        super(1,"Finance");
    }
    @Override
    public void addEmployee(Employee e) {
        Accountant newEmployee=(Accountant) e;
        accountants.add(newEmployee);



        }

    @Override
    public void updateEmployee(Employee e) {
        Accountant updatedEmployee=(Accountant) e;
        int id,departmentId;
        String name, address;
        boolean working;
        boolean exist=false;
        Scanner in=new Scanner(System.in);
        Accountant accountant=null;
        for(int i=0;i<accountants.size();i++) {
            accountant = accountants.get(i);
            if (updatedEmployee.equals(accountant)) {
                exist = true;
                System.out.println("ID: ");
                id = in.nextInt();
                accountant.setId(id);
                System.out.println("Name: ");
                name = in.nextLine();
                accountant.setName(name);
                System.out.println("Address: ");
                address = in.nextLine();
                accountant.setAddress(address);
                System.out.println("WorkingStatus: ");
                working = in.nextBoolean();
                accountant.setWorking(working);
                accountants.set(i, accountant);
                break;
            }
        }
        if(!exist)
        {
            System.out.println("The employee doesn't exist");
        }


    }

    @Override
    public void removeEmployee(Employee e) {
        Accountant newEmployee=(Accountant) e;
        Accountant accountant=null;
        boolean exist=false;
        for(int i=0;i<accountants.size();i++)
        {   accountant=accountants.get(i);
            if(newEmployee.equals(accountant))
            {
                exist=true;
                accountants.remove(newEmployee);
                break;
            }
        }


       if(exist==false) {
           System.out.println("The employee doesn't exist");
       }
    }

    @Override
    public Employee getEmployee(int id) {
        int i=0;
        Accountant accountant=null;
        for(;i<accountants.size();i++)
        {
            accountant=(Accountant) accountants.get(i);
            if(accountant.getId()==id)
                break;
        }
        if(accountant==null)
        {

            System.out.println("No such employee");
        }
        return accountant;
    }

    public void printAll()
    {
        Accountant accountant=null;
        for(int i=0;i< accountants.size();i++)
        {
            accountant=(Accountant)  accountants.get(i);
            System.out.println(accountant.toString());
        }
    }
}
