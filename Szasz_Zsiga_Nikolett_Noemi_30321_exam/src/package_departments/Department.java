package package_departments;

import package_entities.Employee;

public abstract class Department {
    private int id;
    private String name;
    Department(int id,String name)
    {
        this.id=id;
        this.name=name;
    }

    public abstract void addEmployee(Employee e);

    public abstract void removeEmployee(Employee e);


    public abstract void updateEmployee(Employee e);


    public abstract Employee getEmployee(int id);

    public String toString()
    {
        return "ID :"+id +"Name: "+name;
    }

}
