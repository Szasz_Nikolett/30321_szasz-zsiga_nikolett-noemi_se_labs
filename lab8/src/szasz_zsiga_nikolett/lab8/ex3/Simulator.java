package szasz_zsiga_nikolett.lab8.ex3;
import java.util.*;
public class Simulator {
    /**
     * @param args
     */
    public static void main(String[] args) {

        //build station Cluj-Napoca
        Controler c1 = new Controler("Cluj-Napoca");

        Segment s1 = new Segment(1);
        Segment s2 = new Segment(2);
        Segment s3 = new Segment(3);

        c1.addControlledSegment(s1);
        c1.addControlledSegment(s2);
        c1.addControlledSegment(s3);

        //build station Bucuresti
        Controler c2 = new Controler("Bucuresti");

        Segment s4 = new Segment(4);
        Segment s5 = new Segment(5);
        Segment s6 = new Segment(6);

        c2.addControlledSegment(s4);
        c2.addControlledSegment(s5);
        c2.addControlledSegment(s6);

        //build station Iasi
        Controler c3 = new Controler("Iasi");

        Segment s7 = new Segment(7);
        Segment s8 = new Segment(8);
        Segment s9 = new Segment(9);

        c3.addControlledSegment(s7);
        c3.addControlledSegment(s8);
        c3.addControlledSegment(s9);
        //connect the 2 controllers
        Controler arr1[]={c2,c3};
        Controler arr2[]={c1,c3};
        Controler arr3[]={c1,c2};

        c1.setNeighbourController(arr1);
        c2.setNeighbourController(arr2);
        c3.setNeighbourController(arr3);

        //testing

        Train t1 = new Train("Iasi", "IC-001");
        s1.arriveTrain(t1);


        Train t2 = new Train("Cluj-Napoca","R-002");
        s5.arriveTrain(t2);
        s6.arriveTrain(t1);

        Train t3 = new Train("Bucuresti","R-003");
        s8.arriveTrain(t3);

        s2.arriveTrain(t3);

        c1.displayStationState();
        c2.displayStationState();
        c3.displayStationState();
        System.out.println("\nStart train control\n");

        //execute 3 times controller steps
        for(int i = 0;i<3;i++){
            System.out.println("### Step "+i+" ###");
            c1.controlStep();
            c2.controlStep();
            c3.controlStep();
            System.out.println();

            c1.displayStationState();
            c2.displayStationState();
            c3.displayStationState();
        }
    }
}
