package szasz_zsiga_nikolett.lab8.ex3;
import java.util.*;
 class Train {
    String destination;
    String name;

    public Train(String destinatie, String nume) {
        super();
        this.destination = destinatie;
        this.name = nume;
    }

    String getDestination(){
        return destination;
    }
}
