package szasz_zsiga_nikolett.lab8.ex4;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

public class HomeAutomation {
    public static void main(String[] args) throws IOException {
        ControlUnit controlUnit = new ControlUnit();

        //test using an anonymous inner class

        Home h = new Home(){



            protected void setValueInEnvironment(szasz_zsiga_nikolett.lab8.ex4.Event event) {
                System.out.println("New event in environment " + event);
            }




            protected void controllStep(Event event) throws IOException {
                EventType type = event.getType();

                switch (type){
                    case TEMPERATURE:
                        TemperatureEvent temperatureEvent = (TemperatureEvent) event;
                        int val = ((TemperatureEvent) event).getValue();
                        if(val > 20){
                            controlUnit.startCooler();

                        }else if(val < 20){
                            controlUnit.startHeater();
                        }


                        break;
                    case FIRE:
                        for(int i =0; i < 3; i++){
                            FireEvent fireEvent = (FireEvent) event;
                            Random r = new Random();
                            FireEvent fireEvent1 = new FireEvent(r.nextBoolean());
                            FireEvent fireEvent2 = new FireEvent(r.nextBoolean());
                            boolean smoke = ((FireEvent) event).isSmoke();
                            boolean smoke1 = fireEvent1.isSmoke();
                            boolean smoke2 = fireEvent2.isSmoke();
                            if(smoke){
                                controlUnit.startAlarm(0);
                                controlUnit.callOwner();
                            }else if(smoke1){
                                controlUnit.startAlarm(1);
                                controlUnit.callOwner();
                            }else if(smoke2){
                                controlUnit.startAlarm(2);
                                controlUnit.callOwner();
                            }
                        }
                        break;
                    case NONE:
                        NoEvent noEvent = (NoEvent) event;
                        System.out.println(noEvent.toString());
                        break;
                }
            }
        };
        h.simulate();
    }
}