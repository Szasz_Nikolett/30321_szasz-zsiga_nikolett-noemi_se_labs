package edu.utcn.lab12.vehicles;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


public class Parking {

    /**
     * Vehicles will be parked in parkedVehicles array.
     */
    ArrayList<Vehicle> parkedVehicles=new ArrayList<Vehicle>();

    public void parkVehicle(Vehicle e){
        parkedVehicles.add(e);
    }

    /**
     * Sort vehicles by length.
     */
    public void sortByWeight(){
        Collections.sort(parkedVehicles, Comparator.comparing(Vehicle::getWeight));
    }

    public Vehicle get(int index){
        Vehicle vehicle=parkedVehicles.get(index);
        return vehicle;

    }

}
