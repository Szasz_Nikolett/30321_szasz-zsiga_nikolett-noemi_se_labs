package edu.utcn.lab12.vehicles;

public class BatteryException extends Exception {

     public BatteryException(String msg){

        super(msg);
    }

}
