package edu.utcn.lab12.vehicles;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Parking {
    ArrayList<Vehicle> vehicles=new ArrayList<Vehicle>();
    public void parkVehicle(Vehicle e){
        vehicles.add(e);
    }

    /**
     * Sort vehicles by length.
     */
    public void sortByWeight(){

        Collections.sort(vehicles, Comparator.comparing(Vehicle::getWeight));
    }

    public Vehicle get(int index){
        Vehicle vehicle=(Vehicle) vehicles.get(index);
        return vehicle;
    }

}
