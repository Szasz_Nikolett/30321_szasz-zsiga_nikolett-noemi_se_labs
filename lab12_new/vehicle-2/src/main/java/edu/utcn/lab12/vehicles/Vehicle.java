package edu.utcn.lab12.vehicles;

import java.util.Objects;
import java.util.WeakHashMap;

public class Vehicle {

    private String type;
    private int weight;

    public Vehicle(String type, int length) {
        this.type = type;
    }
    public boolean equals(Object obj)
    {
        if(obj instanceof Vehicle)
        {
            Vehicle v=(Vehicle)obj;
            return (type==v.type)&&(weight==v.weight);
        }
        return false;
    }
    @Override
    public int hashCode() {
        return Objects.hash(type, weight);
    }

    public String start(){
        return "engine started";
    }

    public int getWeight()
    {
        return weight;
    }
}
