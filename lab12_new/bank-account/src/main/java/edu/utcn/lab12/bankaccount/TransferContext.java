package edu.utcn.lab12.bankaccount;

public class TransferContext {

    public void transfer(BankAccount sender, BankAccount receiver, int ammount){

        if(sender.getBalance()>ammount)
        {sender.decrease(ammount);
        receiver.increase(ammount);}
        else
            System.out.println("Not possible");

    }

}
