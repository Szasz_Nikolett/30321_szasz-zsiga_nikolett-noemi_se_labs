package edu.utcn.lab12.bankaccount;

import java.util.Objects;
import java.util.logging.Logger;

public class BankAccount {

    public static Logger LOG = Logger.getLogger( BankAccount.class.getName() );

    private String id;
    private int balance;

    public BankAccount(String id, int balance) {
        this.id = id;
        this.balance = balance;
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BankAccount bankAccount = (BankAccount) o;
        return balance == bankAccount.balance &&
                id.equals(bankAccount.id);
    }
    public int hashCode() {
        return Objects.hash(id, balance);
    }
    public void increase(int money) {
        this.balance+=money;
    }

    public void decrease(int money) {
        this.balance -= money;
    }

    public int getBalance() {
        return this.balance;
    }

    public String getId() {
        return this.id;
    }

}
