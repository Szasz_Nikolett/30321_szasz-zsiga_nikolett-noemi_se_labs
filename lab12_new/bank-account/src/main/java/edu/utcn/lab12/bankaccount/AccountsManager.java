package edu.utcn.lab12.bankaccount;

import java.util.ArrayList;

public class AccountsManager {
    ArrayList<BankAccount> accounts=new ArrayList<BankAccount>();
    public void addAccount(BankAccount account){
        accounts.add(account);
    }

    public boolean exists(String id){
    BankAccount bankAccount;
    for(int i=0;i<accounts.size();i++)
    {
        bankAccount=(BankAccount) accounts.get(i);
        if(bankAccount.getId().equals(id)) {
            return true;


        }
    }
    return false;

    }

    public int count(){
        return accounts.size();

    }
}
