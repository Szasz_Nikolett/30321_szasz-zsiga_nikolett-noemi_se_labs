import edu.utcn.lab12.devices.ElectronicDevice;
import org.junit.Test;

import static org.junit.Assert.*;

public class ElectronicDeviceTest {
    @Test
    public void test()
    {
        ElectronicDevice electronicDevice1=new ElectronicDevice();
        electronicDevice1.turnOn();
        assertEquals(true,electronicDevice1.isPowered());
        ElectronicDevice electronicDevice2=new ElectronicDevice();
        electronicDevice2.turnOff();
        assertEquals(false,electronicDevice2.isPowered());
    }
}
