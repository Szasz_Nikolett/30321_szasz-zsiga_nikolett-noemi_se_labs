package szasz_zsiga_nikolett_noemi.lab3.ex1;

public class Robot {
    int x;

    Robot() {
        x = 1;
    }

    public  void change(int k) {
        if (k >= 1) x = x + k;

    }

    public String  toString() {
       return "Position: "+ x;
    }


    public  static void main(String[] args) {
        Robot testRobot=new Robot();
        testRobot.change(5);
        System.out.println(testRobot.toString());

    }
}