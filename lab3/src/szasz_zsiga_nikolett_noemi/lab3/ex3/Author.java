package szasz_zsiga_nikolett_noemi.lab3.ex3;

public class Author {
    private String name,email;
    private char gender;
    public Author(String nume, String mail,char g)
    {
        name=nume;
        email=mail;
        gender=g;
    }
    public String getName()
    {
        return name;
    }
    public String getEmail()
    {
        return email;
    }
    public String setEmail(String mail)
    {   this.email=mail;
        return email;
    }
    public char getGender()
    {
        return gender;
    }
    public String toString()
    {
        return name+" ("+gender+") at "+email;
    }
    public  static void main(String[] args) {
        Author testAuthor=new Author("Szasz Nikolett","sznikolett@utcluj.ro",'f');
        System.out.println("Name:"+testAuthor.getName());
        System.out.println("Email(old)"+testAuthor.getEmail());
        System.out.println("Email(new)"+testAuthor.setEmail("szzsnikolett@utcluj.com"));
        System.out.println("Gender:"+testAuthor.getGender());
        System.out.println(testAuthor.toString());
    }
}
