package szasz_zsiga_nikolett_noemi.lab3.ex4;
import java.lang.Math;
public class MyPoint {
    int x,y;

    MyPoint()
    {x=0;
    y=0;}
    MyPoint(int a,int b)
    {
        x=a;
        y=b;
    }
    public int getX() {
        return x;
    }
    public int getY(){
        return y;
    }
    public int setX(int a)
    {
        this.x=a;
        return x;
    }
    public int setY(int b)
    {
        this.y=b;
        return y;
    }

    public String toString()
    {
        return "("+x+","+y+")";
    }
    public double distance (int x1,int y1)
    {

        int dx=Math.abs(this.x-x1);
        int dy=Math.abs(this.y-y1);
        double distance = Math.sqrt(Math.pow(dx,2)+Math.pow(dy,2));
        return distance;
    }
    public double distance(MyPoint another) {
        int dx = Math.abs(this.x - another.x);
        int dy = Math.abs(this.y - another.y);
        double distance = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));
        return distance;
    }
    public static void main(String[] args)
    {
        MyPoint testMyPoint1=new MyPoint();
        MyPoint testMyPoint2=new MyPoint(1,2);
        System.out.println(testMyPoint1.toString());
        System.out.println(testMyPoint2.toString());
        System.out.println(testMyPoint1.getX()+","+testMyPoint1.getY());
        System.out.println(testMyPoint1.setX(5)+","+testMyPoint2.setY(3));
        System.out.println("Distance (1,5):"+testMyPoint1.distance(1,5));
        System.out.println("Distance point1,2:"+testMyPoint2.distance(testMyPoint1));
    }
}
