package szasz_zsiga_nikolett_noemi.lab3.ex2;

public class Circle {
    private double radius = 1.0;
    private String color = "red";

    Circle(double a) {

        radius = a;

    }

    Circle(double a, String c) {

        radius = a;
        color = c;
    }

    public double getRadius() {
        return radius;
    }

    public double getArea()
    {
        return 3.14*radius*radius;
    }
    public static void main(String[] args)
    {
        Circle testCircle1=new Circle(2.0,"green");
        Circle testCircle2=new Circle(3.0);
        System.out.println("Radius: "+testCircle1.getRadius());
        System.out.println("Area: "+testCircle1.getArea());
        System.out.println("Radius: "+testCircle2.getRadius());
        System.out.println("Area: "+testCircle2.getArea());


    }

}
