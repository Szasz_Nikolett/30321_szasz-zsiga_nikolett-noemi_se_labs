package szasz_zsiga_nikolett_noemi.lab10.ex3;



public class Test {

    public static void main(String[] args) {
        Counter c=new Counter();

        FirstPart f = new FirstPart(c);
        SecondPart s = new SecondPart(c);

        f.start();
        s.start();
    }
}
