package szasz_zsiga_nikolett_noemi.lab10.ex3;

public class SecondPart extends Thread{
    Counter c;
    SecondPart (Counter c)
    {
        this.c=c;
    }
    public void run()
    {
        int start=101;
        int end=200;
        synchronized(c){
            c.count(start,end);
        }

        try { sleep(10);
        } catch (InterruptedException e) {

            e.printStackTrace();
        }
        System.out.println("Finished");
    }
}
