package szasz_zsiga_nikolett_noemi.lab10.ex3;

public class FirstPart extends Thread {
    Counter c;
    FirstPart (Counter c)
    {
        this.c=c;
    }
    public void run()
    {
        int start=1;
        int end=100;
        synchronized(c){
            c.count(start,end);
        }

        try {
            sleep(10);
        } catch (InterruptedException e) {

            e.printStackTrace();
        }
        System.out.println("Finished");
    }
}
