package szasz_zsiga_nikolett_noemi.lab10.ex4;

import java.util.ArrayList;

public class Space {
    ArrayList<Robot> robots=new ArrayList<Robot>();
    ArrayList<Destructor> destructors=new ArrayList<Destructor>();

    public void addRobot(String name)
    {
        Robot robot=new Robot(name);
        robots.add(robot);
    }

    public void addDestructors()
    {
        Robot r1,r2;

        for (int i=0;i<robots.size();i++)
        {
            r1=(Robot) robots.get(i);
            for (int j=0;j<robots.size();j++)
            {
                if(i!=j)
                {
                    r2=(Robot) robots.get(j);
                    Destructor destructor=new Destructor(r1,r2);
                    destructors.add(destructor);
                }
            }


        }
    }
    void startAll()
    {
        Robot robot;
        Destructor destructor;
        for(int i=0;i< robots.size();i++)
        {
            robot= robots.get(i);
            robot.start();
        }
        for(int i=0;i<destructors.size();i++)
        {
            destructor=destructors.get(i);
            destructor.start();
        }
    }
}
