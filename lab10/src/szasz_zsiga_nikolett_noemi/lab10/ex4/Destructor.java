package szasz_zsiga_nikolett_noemi.lab10.ex4;

public class Destructor extends Thread{
    Robot robot1,robot2;
    Destructor(Robot robot1,Robot robot2)
    {
        this.robot1=robot1;
        this.robot2=robot2;
    }
    public void verifyPos()
    {
        if((robot1.getPosx()==robot2.getPosx())&&(robot1.getPosy()==robot2.getPosy()))
        {
            robot1.interrupt();
            robot2.interrupt();


        }
    }
    public void run() {
        try {
            this.verifyPos();
            Thread.sleep(5000); // 5 sec
        } catch (InterruptedException e) {
        }

    }

}
