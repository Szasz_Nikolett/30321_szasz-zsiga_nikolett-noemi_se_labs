package szasz_zsiga_nikolett_noemi.lab10.ex4;
import java.util.Random;

public class Robot extends Thread {
    private int posx, posy;
    Robot (String name)
    {
        super(name);
    }
    public void setPos(int posx,int posy)
    {
        this.posx=posx;
        this.posy=posy;
    }

    public int getPosx() {
        return posx;
    }

    public int getPosy() {
        return posy;
    }


    public void run(){



            try {
                int i =0;
                Random rand=new Random();
                while(++i<15) {
                    int a = rand.nextInt((100) + 1);
                    int b = rand.nextInt((100) + 1);


                    this.setPos(a, b);
                    System.out.println("Current position of the Robot"+getName()+": [" + a + "," + b + "]");
                    sleep(100);
                }
            } catch (InterruptedException e) {

                System.out.println("Interrupted Robot: "+getName());
            }

        }
    }


