package szasz_zsiga_nikolett_noemi.lab10.ex6;
import java.io.ObjectStreamException;
import java.util.*;

public class Chronometer extends Observable  implements Runnable {


    int tmin=0,min=0,tsec=0,sec=0;
    Thread t;
    boolean active=true;
    boolean pause=false;
    boolean reset=false;


    public void start()
    {
        if(t==null)
        {
            t=new Thread(this);
            t.start();
        }
    }

    public void run()
    {
        while(active)
        {
            if(reset||pause)
            {
                if(reset)
                {
                    sec=0;
                    tsec=0;
                    min=0;
                    tmin=0;
                    reset=false;
                }
                if(pause){
                    synchronized(this){
                        try {
                            wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            else {
                if (sec == 9) {
                    sec = 0;
                    if (tsec != 5) {
                        tsec++;
                    } else {
                        tsec = 0;
                        if (min != 9)
                            min++;
                        else {
                            min = 0;
                            if (tmin != 5)
                                tmin++;
                            else tmin = 0;
                        }
                    }
                } else sec++;
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {}

        }

    }
   /* synchronized void go()
    {

        try
        {

         if (sec == 9) {
             sec = 0;
             if (tsec != 5) {
                 tsec++;
             } else {
                 tsec = 0;
                 if (min != 9)
                     min++;
                 else {
                     min = 0;
                     if (tmin != 5)
                         tmin++;
                     else tmin = 0;
                 }
             }
         } else sec++;


        wait(1000);
        }
        catch(Exception e){e.printStackTrace();}
      notify();

    }

    /*synchronized void pause()
    {

        try
        {
            wait();

        }catch(Exception e){e.printStackTrace();}



    }

    synchronized void reset()
    {
        sec=0;
        tsec=0;
        min=0;
        tmin=0;
        this.pause();


    }*/

    public int getSec() {
        return sec;
    }

    public int getTsec() {
        return tsec;
    }

    public int getMin() {
        return min;
    }

    public int getTmin() {
        return tmin;
    }

    public boolean isPause() {
        return pause;
    }

    public boolean isReset() {
        return reset;
    }


    public void setPause(boolean paused) {
      synchronized (this)
      {
          if(paused)
          {
              pause=true;
          }
          else {
              pause=false;
              notify();
          }
      }
    }
    public void setReset()
    {
        synchronized (this)
        {
            reset=true;
        }
    }
}
