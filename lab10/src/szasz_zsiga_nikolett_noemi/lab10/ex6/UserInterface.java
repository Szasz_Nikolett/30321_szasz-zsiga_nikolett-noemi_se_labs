package szasz_zsiga_nikolett_noemi.lab10.ex6;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;
import javax.swing.*;

public class UserInterface extends JPanel implements Observer {
    JTextField timer;
    JLabel tlabel;
    JButton reseter,pause;


    UserInterface()
    {
        this.setLayout(new FlowLayout());
        timer=new JTextField(15);
        tlabel=new JLabel("Time  measured");
        reseter=new JButton("Reset");

        pause=new JButton("Start/Stop");

        add(timer);
        add(tlabel);
        add(reseter);
        add(pause);

    }
    public void update(Observable o, Object arg) {
        String s = "" + ((Chronometer)o).getTmin()+" "+((Chronometer)o).getMin()+" : "+((Chronometer)o).getTsec()+" "+((Chronometer)o).getSec();
        timer.setText(s);
    }
  public void addPauseListener(ButtonControl.PauseButtonListener pauseButtonListener) {
        pause.addActionListener(pauseButtonListener);

    }
    public void addResetListener(ButtonControl.ResetButtonListener resetButtonListener){
        reseter.addActionListener(resetButtonListener);
    }

}
