package szasz_zsiga_nikolett_noemi.lab10.ex6;

import javax.swing.*;
import java.awt.*;

public class ChronometerApp extends JFrame {
    ChronometerApp(UserInterface userInterface)
    {
        setLayout(new BorderLayout());
        add(userInterface,BorderLayout.CENTER);
        pack();
        setVisible(true);
    }
    public static void main(String[] args) {
    Chronometer chronometer=new Chronometer();
    chronometer.start();
    UserInterface userInterface=new UserInterface();
    ButtonControl buttonControl=new ButtonControl(chronometer,userInterface);
    new ChronometerApp(userInterface);


    }
}
