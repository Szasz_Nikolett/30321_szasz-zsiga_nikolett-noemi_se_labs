package szasz_zsiga_nikolett_noemi.lab10.ex6;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ButtonControl {
    Chronometer chronometer;
    UserInterface userInterface;
    public  ButtonControl(Chronometer chronometer,UserInterface userInterface)
    {
        chronometer.addObserver(userInterface);
        this.chronometer=chronometer;
        this.userInterface=userInterface;
        userInterface.addPauseListener(new PauseButtonListener());
        userInterface.addResetListener(new ResetButtonListener());
    }
    class PauseButtonListener implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            chronometer.setPause(!chronometer.isPause());
        }
    }

    class ResetButtonListener implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            chronometer.setReset();
        }
    }
}
