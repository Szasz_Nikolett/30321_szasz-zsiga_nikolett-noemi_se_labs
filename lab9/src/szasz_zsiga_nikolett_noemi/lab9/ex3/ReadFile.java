package szasz_zsiga_nikolett_noemi.lab9.ex3;
import szasz_zsiga_nikolett_noemi.lab9.ex2.Counter;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.*;
public class ReadFile extends JFrame{
    JTextArea tArea;
    JButton open;
    ReadFile()
    {
        setTitle("Test read file");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200,300);
        setVisible(true);

    }

    public void init()
    {
        this.setLayout(null);
        int width=80;int height = 20;

        open = new JButton("Read File!");
        open.setBounds(10,150,width, height);

        open.addActionListener(new ReadFile.Open());

        tArea = new JTextArea();
        tArea.setBounds(10,180,150,80);
        add(tArea);add(open);
    }
    public static void main(String[] args) {
        new ReadFile();
    }
    void opening(String f) {
        try {
            tArea.setText("");
            BufferedReader bf =
                    new BufferedReader(
                            new FileReader(new File(f)));
            String l = "";
            l = bf.readLine();
            while (l != null) {
                tArea.append(l + "\n");
                l = bf.readLine();
            }
        } catch (Exception e) {
            tArea.setText("File not Found");
        }
    }
    class Open implements ActionListener{
        public void actionPerformed(ActionEvent e)
        {
            String fileName="D:\\An2_sem2\\Software_Engineering\\Project\\30321_szasz-zsiga_nikolett-noemi_se_labs\\lab9\\src\\szasz_zsiga_nikolett_noemi\\lab9\\ex3/"+tArea.getText();
            opening(fileName);
        }

        }

}
