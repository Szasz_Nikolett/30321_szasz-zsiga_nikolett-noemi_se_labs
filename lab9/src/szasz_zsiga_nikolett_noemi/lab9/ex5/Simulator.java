package szasz_zsiga_nikolett_noemi.lab9.ex5;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Simulator extends JFrame {
    private static Controler c1,c2,c3;
    private static Segment s1,s2,s3,s4,s5,s6,s7,s8,s9;
    private static JTextArea segment1,segment2,segment3;
    private static JLabel station1;
    private static JLabel station2;
    private static JLabel station3;
    private static JLabel Status;
    private static TextField NewId, NewDestination;
    private static JButton AddTrain;

    Simulator(){
        setTitle("Test Counter");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);
        int width = 300, wSize = 400;
        int height = 80, hSize = 550;
        int x = 80;
        int y = 50;
        station1 = new JLabel("Station1");
        station1.setBounds(wSize/2 - x, 20, width, height);
        segment1 = new JTextArea();
        segment1.setBounds(wSize/2 - 2*x, 20+y, width, height);
        station2 = new JLabel("Station2");
        station2.setBounds(wSize/2 - x, 20+2*y, width, height);
        segment2= new JTextArea();
        segment2.setBounds(wSize/2 - 2*x, 20+3*y, width, height);
        station3 = new JLabel("Station3");
        station3.setBounds(wSize/2 - x, 20+4*y, width, height);
        segment3 = new JTextArea();
        segment3.setBounds(wSize/2 - 2*x, 20+5*y, width, height);

        JLabel destLabel = new JLabel("Destination:");
        destLabel.setBounds(wSize/2-2*x, 20+7*y, 80, height/4);

        JLabel idLabel = new JLabel("Id:");
        idLabel.setBounds(wSize/2-2*x, 20+8*y, 20, height/4);

        Status = new JLabel("Status");
        Status.setBounds(wSize/2-2*x, 440, 80, height/4);

        NewId = new TextField("7");
        NewId.setBounds(wSize/2-x, 20+8*y, width-x, height/4);
        NewDestination = new TextField("Huedin");
        NewDestination.setBounds(wSize/2-x, 20+7*y, width-x, height/4);
        AddTrain = new JButton("Add Train");
        AddTrain.setBounds(wSize/2-2*x, 20+9*y, width, height/4);
        AddTrain.addActionListener(new ButtonAddTrain());

        add(station1);add(station2); add(station3);
        add(segment1);add(segment2);add(segment3);
        add(destLabel); add(idLabel);add(Status);
        add(NewDestination);add(NewId);add(AddTrain);

        setSize(wSize, hSize);
        setVisible(true);

    }

    static class ButtonAddTrain implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            String tDestination;
            int tId;
            if(!NewDestination.getText().equals(" ") && !NewId.getText().equals(" ")){
                tDestination = NewDestination.getText();
                tId = Integer.parseInt(NewId.getText());
                Train t = new Train(tDestination,"R-00"+NewId.getText());
                Status.setText("Added!");
                switch(tId){
                    case 1:
                        s1.arriveTrain(t);
                        break;
                    case 2:
                        s2.arriveTrain(t);
                        break;
                    case 3:
                        s3.arriveTrain(t);
                        break;
                    case 4:
                        s4.arriveTrain(t);
                        break;
                    case 5:
                        s5.arriveTrain(t);
                        break;
                    case 6:
                        s6.arriveTrain(t);
                        break;
                    case 7:
                        s7.arriveTrain(t);
                        break;
                    case 8:
                        s8.arriveTrain(t);
                        break;
                    case 9:
                        s9.arriveTrain(t);
                        break;
                    default:
                        Status.setText("Invalid ID");
                        break;
                }
            }
            segment1.setText(c1.displayStationState()+"\n");
            segment2.setText(c2.displayStationState()+"\n");
            segment3.setText(c3.displayStationState()+"\n");
        }
    }

    public static void main(String[] args) {
        new Simulator();
        //build station Cluj-Napoca
        c1 = new Controler("Cluj-Napoca");
        s1 = new Segment(1);c1.addControlledSegment(s1);
        s2 = new Segment(2);c1.addControlledSegment(s2);
        s3 = new Segment(3);c1.addControlledSegment(s3);

        //build station Bucuresti
        c2 = new Controler("Bucuresti");
        s4 = new Segment(4);c2.addControlledSegment(s4);
        s5 = new Segment(5);c2.addControlledSegment(s5);
        s6 = new Segment(6);c2.addControlledSegment(s6);

        //build station Timisoara
        c3 = new Controler("Timisoara");
        s7 = new Segment(7);c3.addControlledSegment(s7);
        s8 = new Segment(8);c3.addControlledSegment(s8);
        s9 = new Segment(9);c3.addControlledSegment(s9);

        //connect the 3 controllers
        c1.setNeighbourController(c2);
        c1.setNeighbourController(c3);
        c2.setNeighbourController(c1);
        c2.setNeighbourController(c3);
        c3.setNeighbourController(c1);

        //testing
        Train t1 = new Train("Bucuresti", "IC-001");
        s1.arriveTrain(t1);

        Train t2 = new Train("Timisoara","R-002");
        s5.arriveTrain(t2);

        Train t3 = new Train("Cluj-Napoca","IR-003");
        s9.arriveTrain(t3);

        station1.setText("=== STATION " + c1.stationName + " ===");
        station2.setText("=== STATION " + c2.stationName + " ===");
        station3.setText("=== STATION " + c3.stationName + " ===");

        segment1.append(c1.displayStationState()+"\n");
        segment2.append(c2.displayStationState()+"\n");
        segment3.append(c3.displayStationState()+"\n");

        AddTrain.addActionListener(new ButtonAddTrain());


    }
}