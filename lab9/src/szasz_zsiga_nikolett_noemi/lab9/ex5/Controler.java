package szasz_zsiga_nikolett_noemi.lab9.ex5;
import java.util.*;
import java.util.ArrayList;

class Controler {
    String stationName;
    ArrayList<Controler> cont = new ArrayList<>();
    //storing station train track segments
    ArrayList<Segment> list = new ArrayList<>();

    public Controler(String gara) {
        stationName = gara;
    }

    void setNeighbourController(Controler v) {
        cont.add(v);
    }

    void addControlledSegment(Segment s) {
        list.add(s);
    }

    int getFreeSegmentId() {
        for (Segment s : list) {
            if (!s.hasTrain())
                return s.id;
        }
        return -1;
    }

    String controlStep() {
        //check which train must be sent
        int i;
        for (Segment segment : list) {
            if (segment.hasTrain()) {
                Train t = segment.getTrain();
                for(i =0; i<cont.size(); i++){
                    if (t.getDestination().equals(cont.get(i).stationName)) {
                        //check if there is a free segment
                        int id = cont.get(i).getFreeSegmentId();
                        if (id == -1) {
                            return "Trenul +" + t.name + "din gara " + stationName + " nu poate fi trimis catre " + cont.get(i).stationName + ". Nici un segment disponibil!";
                        }
                        //send train
                        segment.departTRain();
                        cont.get(i).arriveTrain(t, id);
                        return "Trenul " + t.name + " pleaca din gara " + stationName + " spre gara " + cont.get(i).stationName;
                    }
                }

            }
        }
        return " ";
    }

    public String arriveTrain(Train t, int idSegment) {
        for (Segment segment : list) {
            //search id segment and add train on it
            if (segment.id == idSegment)
                if (segment.hasTrain()) {
                    return "CRASH! Train " + t.name + " colided with " + segment.getTrain().name + " on segment " + segment.id + " in station " + stationName;
                } else {
                    segment.arriveTrain(t);
                    return "Train " + t.name + " arrived on segment " + segment.id + " in station " + stationName;
                }
        }
        //this should not happen
        return "Train " + t.name + " cannot be received " + stationName + ". Check controller logic algorithm!";
    }

    public StringBuilder displayStationState() {
        StringBuilder Output = new StringBuilder();
        for (Segment s : list) {
            if (s.hasTrain())
                Output.append("|----------ID=").append(s.id).append("__Train=").append(s.getTrain().name).append(" to ").append(s.getTrain().destination).append("__----------|").append("\n");
            else
                Output.append("|----------ID=").append(s.id).append("__Train=______ catre ________----------|").append("\n");
        }
        return Output;
    }
}
