package szasz_zsiga_nikolett_noemi.lab9.ex4;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import java.util.*;

public class Game extends JFrame {
    JButton buttons[] = new JButton[9];
    boolean player = true;
    int array[]=new int[9];
    int counter=0;

    Game() {
        setTitle("X&O");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(400, 400);
        setVisible(true);
    }

    public void init() {
        setLayout(new GridLayout(3, 3));
        add(buttons[0] = new JButton(" "));
        add(buttons[1] = new JButton(" "));
        add(buttons[2] = new JButton(" "));

        add(buttons[3] = new JButton(" "));
        add(buttons[4] = new JButton(" "));
        add(buttons[5] = new JButton(" "));

        add(buttons[6] = new JButton(" "));
        add(buttons[7] = new JButton(" "));
        add(buttons[8] = new JButton(" "));


        for (int i =0 ; i <= 8; i++)
            buttons[i].addActionListener(new Game.ButtonClicked());
        for(int i=0;i<9;i++)
            array[i]=0;
        pack();
        setVisible(true);
    }


    public static void main(String[] args) {
        new Game();
    }
    int verify()
    {  int sum=0,winner=0;



               for (int i = 0; i < 3; i++) {
                   sum = 0;
                   sum = array[i] + array[i + 3] + array[i + 6];
                   if (sum == 3) {
                       winner = 1;
                       break;
                   } else if (sum == -3) {
                       winner = -1;
                       break;
                   }
               }

           if (winner==0) {
               for (int i=0; i <6; i += 3)
               {
                   sum=0;
                   sum = array[i] + array[i + 1] + array[i + 2];
                   if (sum == 3) {
                       winner = 1;
                       break;
                   } else if (sum == -3) {
                       winner = -1;
                       break;
                   }
               }

           }
           if(winner==0)
           {sum=array[0]+array[4]+array[8];
            if (sum == 3)
            winner = 1;
            else if (sum == -3)
                    winner = -1;
            else
            { sum=array[2]+array[4]+array[6];
               if (sum == 3)
                   winner = 1;
               else if (sum == -3)
                   winner = -1;}

       }
           return winner;
    }
    class ButtonClicked implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            int number=0;

            JButton button=buttons[0];
           for(int i=0;i<9;i++)
               if(e.getSource()==buttons[i])
               {number=i;
               button=buttons[i];
               break;}
           counter++;
           if(player==true&&array[number]==0)
           {
               button.setText("X");
               array[number]=1;
               player=false;

           }
           else if(player==false&&array[number]==0)
           {
               button.setText("O");
               array[number]=-1;
               player=true;
           }
           else if(array[number]!=0&&counter!=9)
               System.out.println("Chose another field");

           if(verify()!=0||counter==9)
           {
               if(verify()==1)
                   System.out.println("X wins");
               else if(verify()==-1)
                   System.out.println("O wins");
               else
                   System.out.println("TIE");
               System.exit(0);
           }

        }

    }
}
