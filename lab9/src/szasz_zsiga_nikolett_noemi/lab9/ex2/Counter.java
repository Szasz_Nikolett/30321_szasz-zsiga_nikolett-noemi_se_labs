package szasz_zsiga_nikolett_noemi.lab9.ex2;
import szasz_zsiga_nikolett_noemi.lab9.ex1.ButtonAndTextField2;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import java.util.*;

public class Counter extends JFrame {
    JTextArea tArea;
    JButton count;
    int counter=0;
    Counter()
    {
        setTitle("Test counter");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200,300);
        setVisible(true);

    }
    public void init()
    {
        this.setLayout(null);
        int width=80;int height = 20;

        count = new JButton("Add!");
        count.setBounds(10,150,width, height);

        count.addActionListener(new Counter.TratareButonCount());

        tArea = new JTextArea();
        tArea.setBounds(10,180,150,80);
        add(tArea);add(count);
    }
    public static void main(String[] args) {
        new Counter();
    }

    class TratareButonCount implements ActionListener{
        public void actionPerformed(ActionEvent e)
        {
            counter++;
            tArea.setText(String.valueOf(counter));
        }
    }
}
