package szasz_zsiga_nikolett.lab7.ex3;

import java.io.*;

public class TextEdit {
    public void encryption  (File file) throws IOException
    {   BufferedReader in=null;
        int numberChar=0;
        File file2 = new File("D:\\An2_sem2\\Software_Engineering\\Project\\30321_szasz-zsiga_nikolett-noemi_se_labs\\lab7\\src\\szasz_zsiga_nikolett\\lab7\\ex3/result.enc");
        file2.createNewFile();
        FileReader fr = new FileReader(file);
        FileWriter wr=new FileWriter(file2);
        in=new BufferedReader(fr);
        String line=in.readLine();

        int j;
        while (line!=null)
        {
            char[] ch1=new char[line.length()];
            char[] ch2=new char[line.length()];
            for(int i=0;i<line.length();i++)
            {
                ch1[i]=line.charAt(i);
                j=(int)ch1[i];
                ch2[i]=(char)(j+1);
            }
            String lineEncrypted=new String(ch2);
            wr.write(lineEncrypted);
            line=in.readLine();
        }
        fr.close();
        wr.close();
        in.close();
    }
    public void decryption  (File file) throws IOException
    {   BufferedReader in=null;
        int numberChar=0;
        File file2 = new File("D:\\An2_sem2\\Software_Engineering\\Project\\30321_szasz-zsiga_nikolett-noemi_se_labs\\lab7\\src\\szasz_zsiga_nikolett\\lab7\\ex3/result.dec");
        file2.createNewFile();
        FileReader fr = new FileReader(file);
        FileWriter wr=new FileWriter(file2);
        in=new BufferedReader(fr);
        String line=in.readLine();

        int j;
        while (line!=null)
        {
            char[] ch1=new char[line.length()];
            char[] ch2=new char[line.length()];
            for(int i=0;i<line.length();i++)
            {
                ch1[i]=line.charAt(i);
                j=(int)ch1[i];
                ch2[i]=(char)(j-1);
            }
            String lineEncrypted=new String(ch2);
            wr.write(lineEncrypted);
            line=in.readLine();
        }
        fr.close();
        wr.close();
        in.close();
    }
}
