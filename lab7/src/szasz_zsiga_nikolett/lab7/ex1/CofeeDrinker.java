package szasz_zsiga_nikolett.lab7.ex1;

public class CofeeDrinker {
    void drinkCofee(Cofee c) throws TemperatureException, ConcentrationException,ObjectNumberException{
        if(c.getObjectCreated()>5)
            throw new ObjectNumberException(c.getObjectCreated(),"Too much coffee for today!");
        if(c.getTemp()>60)
            throw new TemperatureException(c.getTemp(),"Cofee is to hot!");
        if(c.getConc()>50)
            throw new ConcentrationException(c.getConc(),"Cofee concentration to high!");

        System.out.println("Drink cofee:"+c);
    }
}
