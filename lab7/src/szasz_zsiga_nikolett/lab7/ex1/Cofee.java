package szasz_zsiga_nikolett.lab7.ex1;

public class Cofee {
    private int temp;
    private int conc;
    private static int objectCreated=0;
    Cofee(int t,int c){temp = t;conc = c; objectCreated++;}
    int getTemp(){return temp;}
    int getConc(){return conc;}
    int getObjectCreated(){return objectCreated;}
    public String toString(){return "[cofee temperature="+temp+":concentration="+conc+"]";}
}
