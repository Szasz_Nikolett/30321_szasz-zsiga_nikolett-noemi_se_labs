package szasz_zsiga_nikolett.lab7.ex1;

public class ObjectNumberException extends Exception{
    int o;
    public ObjectNumberException(int o,String msg) {
        super(msg);
        this.o = o;
    }

    int getObjectCreated(){
        return o;
    }
}
