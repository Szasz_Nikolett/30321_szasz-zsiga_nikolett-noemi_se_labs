package szasz_zsiga_nikolett.lab7.ex2;
import java.io.*;

public class CharCount {
    private char c;

    CharCount(char c) {
        this.c = c;
    }

    public int charNumber() throws IOException {
        int i = 0;
        File file = new File("D:\\An2_sem2\\Software_Engineering\\Project\\30321_szasz-zsiga_nikolett-noemi_se_labs\\lab7\\src\\szasz_zsiga_nikolett\\lab7\\ex2/data.txt");
        FileReader fr = new FileReader(file);
        char[] arr = new char[100];
        fr.read(arr);
        for (int j = 0; j < arr.length; j++) {
            if (arr[j] == this.c) i++;
        }
        fr.close();
        return i;
    }
}
