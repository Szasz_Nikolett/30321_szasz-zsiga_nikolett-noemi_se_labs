package szasz_zsiga_nikolett.lab7.ex2;
import java.io.*;
import java.util.Scanner;

public class CountTest {
    public static void main(String[] args) throws IOException {
        char x;
        Scanner in=new Scanner(System.in);
        System.out.println("Please provide a character you want to count: ");
        x=in.nextLine().toCharArray()[0];
        int number=new CharCount(x).charNumber();
        System.out.println("The document contains the letter "+x+" for "+number+" times");
    }
}
