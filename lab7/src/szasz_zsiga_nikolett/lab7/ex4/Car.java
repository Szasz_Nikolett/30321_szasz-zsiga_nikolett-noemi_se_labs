package szasz_zsiga_nikolett.lab7.ex4;
import java.util.*;
import java.io.*;
public class Car implements Serializable {
    public String model;
    public double price;
    public Car(String model,double price)
    {
        this.price=price;
        this.model=model;
    }
     public String getModel()
     {
         return model;
     }
     public double getPrice()
     {
         return price;
     }
}
