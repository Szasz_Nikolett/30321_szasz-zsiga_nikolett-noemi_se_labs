package szasz_zsiga_nikolett.lab7.ex4;
import java.io.*;
public class Deserialization {
    public static void main(String [] args) {
        Car car1 = null;
        Car car2 = null;
        Car car3 = null;
        try {
            FileInputStream file=new FileInputStream("D:\\An2_sem2\\Software_Engineering\\Project\\30321_szasz-zsiga_nikolett-noemi_se_labs\\lab7\\src\\szasz_zsiga_nikolett\\lab7\\ex4\\Cars/cars1.ser");

            ObjectInputStream in = new ObjectInputStream(file);
            car1 = (Car) in.readObject();
            in.close();
            file.close();
            FileInputStream file2=new FileInputStream("D:\\An2_sem2\\Software_Engineering\\Project\\30321_szasz-zsiga_nikolett-noemi_se_labs\\lab7\\src\\szasz_zsiga_nikolett\\lab7\\ex4\\Cars/cars2.ser");
            in = new ObjectInputStream(file2);
            car2 = (Car) in.readObject();
            in.close();
            file2.close();
            FileInputStream file3=new FileInputStream("D:\\An2_sem2\\Software_Engineering\\Project\\30321_szasz-zsiga_nikolett-noemi_se_labs\\lab7\\src\\szasz_zsiga_nikolett\\lab7\\ex4\\Cars/cars3.ser");
            in = new ObjectInputStream(file3);
            car3 = (Car) in.readObject();
            in.close();
            file3.close();
        } catch (IOException i) {
            i.printStackTrace();
            return;
        } catch (ClassNotFoundException c) {
            System.out.println("Employee class not found");
            c.printStackTrace();
            return;
        }

        System.out.println("Deserialized Car...\n");
        System.out.println("Model: " + car1.model+"\n");
        System.out.println("Price: "+car1.price+"\n");
        System.out.println("Deserialized Car...\n");
        System.out.println("Model: " + car2.model+"\n");
        System.out.println("Price: "+car2.price+"\n");
        System.out.println("Deserialized Car...\n");
        System.out.println("Model: " + car3.model+"\n");
        System.out.println("Price: "+car3.price+"\n");
    }
}
