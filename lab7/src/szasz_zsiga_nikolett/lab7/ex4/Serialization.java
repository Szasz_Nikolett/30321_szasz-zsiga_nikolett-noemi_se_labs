package szasz_zsiga_nikolett.lab7.ex4;
import java.io.*;
public class Serialization {
    public static void main(String [] args) {
        Car car1=new Car("Ford",1234.2);
        Car car2=new Car("Skoda",1000.5);
        Car car3=new Car("Smart",988.0);
        try {
            FileOutputStream file=new FileOutputStream("D:\\An2_sem2\\Software_Engineering\\Project\\30321_szasz-zsiga_nikolett-noemi_se_labs\\lab7\\src\\szasz_zsiga_nikolett\\lab7\\ex4\\Cars/cars1.ser");
            ObjectOutputStream output=new ObjectOutputStream(file);
            output.writeObject(car1);
            output.close();
            file.close();
            System.out.printf("Serialized data is saved in cars1.ser");
            FileOutputStream file2=new FileOutputStream("D:\\An2_sem2\\Software_Engineering\\Project\\30321_szasz-zsiga_nikolett-noemi_se_labs\\lab7\\src\\szasz_zsiga_nikolett\\lab7\\ex4\\Cars/cars2.ser");
            output=new ObjectOutputStream(file2);
            output.writeObject(car2);
            output.close();
            file.close();
            System.out.printf("Serialized data is saved in cars2.ser");
            FileOutputStream file3=new FileOutputStream("D:\\An2_sem2\\Software_Engineering\\Project\\30321_szasz-zsiga_nikolett-noemi_se_labs\\lab7\\src\\szasz_zsiga_nikolett\\lab7\\ex4\\Cars/cars3.ser");
            output=new ObjectOutputStream(file3);
            output.writeObject(car3);
            output.close();
            file.close();
            System.out.printf("Serialized data is saved in cars3.ser");
        }catch (IOException i) {
            i.printStackTrace();
        }
    }
}
