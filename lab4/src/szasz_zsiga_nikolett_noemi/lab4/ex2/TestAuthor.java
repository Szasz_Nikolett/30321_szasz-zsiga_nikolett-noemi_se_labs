package szasz_zsiga_nikolett_noemi.lab4.ex2;

public class TestAuthor {
    public  static void main(String[] args) {
        Author testAuthor=new Author("Szasz Nikolett","sznikolett@utcluj.ro",'f');
        System.out.println("Name:"+testAuthor.getName());
        System.out.println("Email(old)"+testAuthor.getEmail());
        System.out.println("Email(new)"+testAuthor.setEmail("szzsnikolett@utcluj.com"));
        System.out.println("Gender:"+testAuthor.getGender());
        System.out.println(testAuthor.toString());
    }
}
