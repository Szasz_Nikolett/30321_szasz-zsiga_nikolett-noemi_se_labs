package szasz_zsiga_nikolett_noemi.lab4.ex5;
import szasz_zsiga_nikolett_noemi.lab4.ex1.Circle;
public class Cylinder extends Circle  {
    private double height=1.0;

    public Cylinder()
    {
        super();
        this.height=1.0;

    }

    public Cylinder( double radius)
    {
        super(radius);
        this.height=1.0;

    }
    public Cylinder (double radius, double height)
    {
        super(radius);
        this.height=height;
    }

    public double getHeight()
    {
        return height;
    }


    public double getArea()
    {
        return 2*3.14*this.getRadius()*this.height;
    }
    public double getVolume()
    {
        return this.getArea()*this.height;
    }

}
