package szasz_zsiga_nikolett_noemi.lab4.ex5;

public class CylinderTest {
    public static void main(String[] args) {

        Cylinder testCylinder1=new Cylinder();
        Cylinder testCylinder2=new Cylinder(2.0);
        Cylinder testCylinder3=new Cylinder(1.5,2.5);
        System.out.println(testCylinder1.getHeight()+" "+testCylinder2.getHeight()+" "+testCylinder3.getHeight());
        System.out.println(testCylinder1.getArea()+" "+testCylinder2.getArea()+" "+testCylinder3.getArea());
        System.out.println(testCylinder1.getVolume()+" "+testCylinder2.getVolume()+" "+testCylinder3.getVolume());

    }

    }

