package szasz_zsiga_nikolett_noemi.lab4.ex4;

import  szasz_zsiga_nikolett_noemi.lab4.ex2.Author;
public class Book {
    private String name;
    private Author[] authors;
    private double price;
    private int qtyInStock = 0;

    public Book(String name, Author[] authors, double price) {


        this.name = name;
        this.price = price;
        this.authors=authors;

    }

    public Book(String name, Author[] authors, double price, int qtyInStock) {


        this.name = name;
        this.price = price;
        this.qtyInStock = qtyInStock;
        this.authors=authors;


    }

    public String getName() {
        return name;
    }

    public Author[] getAuthors() {
        return authors;
    }

    public double getPrice() {
        return price;
    }
    public double setPrice(double price)
    {
        this.price=price;
        return this.price;
    }

    public int getQtyInStock() {
        return this.qtyInStock;
    }

    public int setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
        return this.qtyInStock;
    }

    public String toString() {
        return name +" by "+this.authors.length+" authors ";
    }
    public void printAuthors(){
        for(int i=0;i<this.authors.length;i++)
            System.out.println(this.authors[i]);
    }

}
