package szasz_zsiga_nikolett_noemi.lab4.ex4;

import szasz_zsiga_nikolett_noemi.lab4.ex2.Author;


public class TestBook {
    public  static void main(String[] args) {
        Author testAuthor1=new Author("J,K.Rowling","jkrowl@gmail.com",'f');
        Author testAuthor2=new Author("Emily Bronte","bemily@yahoo.com",'f');
        Author[] testArray=new Author[2];
        testArray[0]=testAuthor1;
        testArray[1]=testAuthor2;
        Book testBook1= new Book("Harry Potter",testArray,35.0);
        Book testBook2=new Book("Wuthering Heights",testArray,25.2,25);
        System.out.println(testBook1.getName());
        System.out.println(testBook2.getName());
        testBook1.printAuthors();
        testBook2.printAuthors();
        System.out.println("PRICE: "+testBook1.getPrice());
        System.out.println("PRICE: "+testBook2.getPrice());
        System.out.println("PRICE: "+testBook1.setPrice(27.3));
        System.out.println("PRICE: "+testBook2.setPrice(35.2));
        System.out.println("Quantity "+testBook1.getQtyInStock());
        System.out.println("Quantity "+testBook2.getQtyInStock());
        System.out.println("Quantity "+testBook1.setQtyInStock(20));
        System.out.println("Quantity "+testBook2.setQtyInStock(10));
        System.out.println(testBook1.toString());
        System.out.println((testBook2.toString()));
    }
}

