package szasz_zsiga_nikolett_noemi.lab4.ex3;
import  szasz_zsiga_nikolett_noemi.lab4.ex2.Author;
public class Book {
    private String name;
    private Author author;
    private double price;
    private int qtyInStock = 0;

    public Book(String name, Author author, double price) {


        this.name = name;
        this.price = price;
        this.author=author;

    }

    public Book(String name, Author author, double price, int qtyInStock) {


        this.name = name;
        this.price = price;
        this.qtyInStock = qtyInStock;
        this.author=author;


    }

    public String getName() {
        return name;
    }

    public Author getAuthor() {
        return author;
    }

    public double getPrice() {
        return price;
    }
    public double setPrice(double price)
    {
        this.price=price;
        return this.price;
    }

    public int getQtyInStock() {
        return this.qtyInStock;
    }

    public int setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
        return this.qtyInStock;
    }

    public String toString() {
        return name +" "+ super.toString();
    }
}
