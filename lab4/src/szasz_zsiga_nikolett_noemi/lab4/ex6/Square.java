package szasz_zsiga_nikolett_noemi.lab4.ex6;

public class Square extends Rectangle{
    public Square()
    {
        width=length=1.0;
    }
    public Square( double side)
    {
        length=side;
        width=side;
    }
    public Square(double side, String color, boolean filled)
    {
        length=side;
        width=side;
        this.color=color;
        this.filled=filled;
    }

    public double getSide()
    {
        return length;
    }
    public void setSide(double side)
    {
        this.length=side;
        this.width=side;


    }
    public void setWidth (double side)
    {
        this.length=side;
        this.width=side;

    }

    public void setLength(double side)
    {
        this.length=side;
        this.width=side;

    }

    public String toString()
    {
        return "A Square with side="+this.width+ ", which is a subclass of"+super.toString();
    }

}
