package szasz_zsiga_nikolett_noemi.lab4.ex6;

public class Circle extends Shape{

    double radius;
    public Circle()
    {
        radius=1.0;
    }
    public Circle(double radius)
    {
        this.radius=radius;

    }
    public Circle(double radius,String color,boolean filled)
    {
        this.radius=radius;
        this.color=color;
        this.filled=filled;
    }

    public double getRadius() {
        return radius;
    }
    public void setRadius(double radius){
            this.radius=radius;


    }
    public double getArea()
    {
        return 2*3.14*radius*radius;
    }
    public double getPerimeter()
    {
        return 2*3.14*radius;
    }

    public String toString()
    {
        return "A Circle with radius= " +this.radius+ "which is a subclass of "+super.toString();
    }
}
