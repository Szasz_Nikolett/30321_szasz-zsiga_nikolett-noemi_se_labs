package szasz_zsiga_nikolett_noemi.lab4.ex6;

public class Test {
    public static void main(String[] args) {
        Shape testShape1 = new Shape();
        Shape testShape2 = new Shape("blue", false);
        Circle testCircle1 = new Circle();
        Circle testCircle2 = new Circle(1.0);
        Circle testCircle3 = new Circle(2.0, "green", true);
        Rectangle testRectangle1 = new Rectangle();
        Rectangle testRectangle2 = new Rectangle(2.5, 3.0);
        Rectangle testRectangle3 = new Rectangle(1.5, 3.0, true);
        Square testSquare1 = new Square();
        Square testSquare2 = new Square(2.5);
        Square testSquare3 = new Square(3.0, "orange", false);
        System.out.println(testShape1.getColor() + " " + testShape2.getColor() + " " + testSquare3.getColor());
        System.out.println(testCircle1.getColor() + " " + testCircle2.getColor() + " " + testCircle3.getColor());
        System.out.println(testRectangle1.getColor() + " " + testRectangle2.getColor() + " " + testRectangle3.getColor());
        System.out.println(testSquare1.getColor() + " " + testSquare2.getColor() + " " + testSquare3.getColor());
        System.out.println(testShape1.toString());
        System.out.println(testCircle1.toString());
        System.out.println(testRectangle1.toString());
        System.out.println(testSquare1.toString());
        testSquare1.setSide(1.0);
        testSquare2.setWidth(1.5);
        testSquare3.setWidth(2.5);
        System.out.println(testSquare1.getSide() + " " + testSquare2.getSide() + " " + testSquare3.getSide());
        System.out.println(testSquare1.getArea() + " " + testSquare1.getPerimeter());
        testRectangle1.setLength(1.5);
        testRectangle1.setWidth(2.8);
        System.out.println(testRectangle1.getArea() + " " + testRectangle1.getPerimeter());
        System.out.println(testSquare3.isFILLED());
        testSquare2.setFILLED(false);
        System.out.println(testSquare2.isFILLED());
        System.out.println("Radius: " + testCircle1.getRadius());
        System.out.println("Area: " + testCircle1.getArea());
        testCircle2.setRadius(1.5);
        System.out.println("Radius: " + testCircle2.getRadius());
        testCircle3.setColor("purple");
        System.out.println(testCircle3.getColor());
    }

    }

