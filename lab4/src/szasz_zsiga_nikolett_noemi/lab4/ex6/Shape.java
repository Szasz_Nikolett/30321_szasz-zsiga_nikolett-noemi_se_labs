package szasz_zsiga_nikolett_noemi.lab4.ex6;

public class Shape {
    String color;
    boolean filled;

    public Shape()
    {
        color="green";
        filled=true;
    }

    public Shape( String color,boolean filled)
    {
        this.color=color;
        this.filled=filled;
    }
    public String getColor( )
    {
        return color;
    }
    public void setColor(String color)
    {
        this.color=color;

    }
    public boolean isFILLED()
    {
        return filled;
    }
    public void setFILLED(boolean filled)
    {
        this.filled=filled;

    }

    public String toString()
    {
        return "A shape with color of "+this.color+" and "+filled;
    }
}
