package szasz_zsiga_nikolett_noemi.lab4.ex6;

public class Rectangle extends Shape{
    double width;
    double length;

    public Rectangle()
    {
        width=1.0;
        length= 1.0;
    }
    public Rectangle(double width, double length)
    {
    this.width=width;
    this.length=length;

    }
    public Rectangle (double width, double length, boolean filled)
    {
        this.width=width;
        this.length=length;
        this.filled=filled;
    }

    public double getWidth() {
        return width;
    }
    public void setWidth(double width)
    {
        this.width=width;

    }
    public double getLength()
    {
        return length;
    }
    public void setLength(double length)
    {
        this.length=length;

    }
    public double getArea()
    {
        return length*width;
    }
    public double getPerimeter()
    {
        return 2*(length+width);
    }
    public String toString()
    {
        return "A Rectangle with width= " +this.width+" and length "+ this.length+ "which is a subclass of "+super.toString();
    }
}

