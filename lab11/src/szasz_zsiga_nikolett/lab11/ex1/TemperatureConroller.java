package szasz_zsiga_nikolett.lab11.ex1;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class TemperatureController  {
    Thermometer t;
    TemperatureTextView tview;
    public TemperatureController(Thermometer t, TemperatureTextView tview){
        t.addObserver(tview);
        this.t = t;
        this.tview = tview;

        tview.addEnableDisableListener(new EnableDisableListener());
    }

    class EnableDisableListener implements ActionListener{

        public void actionPerformed(ActionEvent e) {
            t.setPause(!t.isPaused());
        }

    }
}