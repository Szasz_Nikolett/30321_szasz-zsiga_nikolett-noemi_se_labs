package szasz_zsiga_nikolett.lab6.ex2;

public class Test {
    public static void main(String[] args) {

        Bank bank=new Bank();
        bank.addAccount("Franklin",10034.23);
        bank.addAccount("Karen",2550.10);
        bank.addAccount("John",1550.20);
        bank.printAccounts();
        bank.printAccounts(2000,5000);
        BankAccount test1=bank.getAccount("Dan");
        System.out.println(test1.toString());
        bank.getAllAccounts();
    }
}
