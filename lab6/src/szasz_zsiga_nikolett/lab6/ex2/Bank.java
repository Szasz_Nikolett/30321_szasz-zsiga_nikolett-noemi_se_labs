package szasz_zsiga_nikolett.lab6.ex2;

import java.util.*;

public class Bank {
    private ArrayList<BankAccount> accounts=new ArrayList();
    public void addAccount( String owner, double balance)
    {
        BankAccount bankAccount=new BankAccount(owner,balance);
        accounts.add(bankAccount);
    }

    public void printAccounts()
    {
        BankAccount test1;
        Collections.sort(accounts, Comparator.comparing(BankAccount::getBalance));
        for(int i=0;i<accounts.size();i++)
        {
            test1=(BankAccount) accounts.get(i);
            System.out.println(test1.toString());

        }

    }
    public void printAccounts(double minBalance,double maxBalance)
    {   BankAccount test1;
        for(int i=0;i<accounts.size();i++)
        {
            test1=(BankAccount) accounts.get(i);
            if(test1.getBalance()<maxBalance&&test1.getBalance()>minBalance)
                System.out.println(test1.toString());
        }
    }
    public BankAccount getAccount(String owner)
    {   Scanner in=new Scanner(System.in);
        BankAccount test1;
        int i;
        for(i=0;i<accounts.size();i++) {
            test1 = (BankAccount) accounts.get(i);
            if (test1.getOwner().equals(owner))
                break;
        }

        if(i<accounts.size())
        {   test1=(BankAccount) accounts.get(i);
            return test1;
        }
        else
        {
            System.out.println("The bank account does not exist,please create it by adding the balance");
            double balance_read=in.nextDouble();
            this.addAccount(owner,balance_read);
            test1=(BankAccount) accounts.get(i);
            return test1;

        }
    }
    public void getAllAccounts()
    {
        BankAccount test1;
        Collections.sort(accounts, Comparator.comparing(BankAccount::getOwner));
        for(int i=0;i<accounts.size();i++)
        {
            test1=(BankAccount) accounts.get(i);
            System.out.println(test1.toString());

        }

    }


}
