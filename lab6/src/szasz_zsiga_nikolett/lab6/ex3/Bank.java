package szasz_zsiga_nikolett.lab6.ex3;

import java.util.*;

public class Bank {
    private TreeSet<BankAccount> accounts=new TreeSet();
    public void addAccount( String owner, double balance)
    {
        BankAccount bankAccount=new BankAccount(owner,balance);
        accounts.add(bankAccount);
    }

    public void printAccounts()
    {
        BankAccount test1;
        Iterator it=accounts.iterator();
       while(it.hasNext())
       { test1=(BankAccount) it.next();
       System.out.println(test1.toString());

        }

    }
    public void printAccounts(double minBalance,double maxBalance)
    {   BankAccount test1;
        Iterator it=accounts.iterator();
        while(it.hasNext())
        { test1=(BankAccount) it.next();

                if(test1.getBalance()<maxBalance&&test1.getBalance()>minBalance)
                    System.out.println(test1.toString());
        }
    }
    public BankAccount getAccount(String owner)
    {   Scanner in=new Scanner(System.in);
        BankAccount test1 = null;
        Iterator it=accounts.iterator();
        boolean found=false;
        while(it.hasNext()&&!found) {
            test1 = (BankAccount) it.next();
            if (test1.getOwner().equals(owner)) {
                found = true;

            }
        }
       if(!found)
        {
            System.out.println("The bank account does not exist,please create it by adding the balance");
            double balance_read=in.nextDouble();
            this.addAccount(owner,balance_read);
            found=false;
            it=accounts.iterator();
            while(it.hasNext()&&!found) {
                test1 = (BankAccount) it.next();
                if (test1.getOwner().equals(owner)) {
                    found = true;

                }
            }

        }
        return test1;
    }
    public void getAllAccounts()
    {
        BankAccount test1;
        String [] names=new String[accounts.size()];
        Iterator it=accounts.iterator();
        int i=0;
        while(it.hasNext())
        { test1=(BankAccount) it.next();
          names[i++]= test1.getOwner();

        }
        Arrays.sort(names);
        for(i=0;i<accounts.size();i++)
            System.out.println(this.getAccount(names[i]).toString());

    }


}
