package szasz_zsiga_nikolett.lab6.ex3;

public class BankAccount implements Comparable {
    private String owner;
    private double balance;
    public boolean equals(Object o){
        if(o==null||!(o instanceof BankAccount) )
            return false;
        BankAccount x = (BankAccount) o;
        return x.owner.equals(owner)&&x.balance==balance;
    }
    public BankAccount(String owner,double balance)
    {
        this.owner=owner;
        this.balance=balance;
    }
    public int hashCode(){
        return (int)balance + owner.hashCode();
    }

    public void withdraw(double amount)
    {
        System.out.println("Your balance before withdrawal : "+this.balance);
        this.balance=this.balance-amount;
        System.out.println("Your balance after withdrawal : "+this.balance);
    }
    public void deposit (double amount)
    {   System.out.println("Your balance before depositing: "+this.balance);
        this.balance=this.balance+amount;
        System.out.println("Your balance after depositing: "+this.balance);
    }

    public double getBalance() {
        return balance;
    }

    public String getOwner() {
        return owner;
    }

    public String toString()
    {
        return "Owner: "+this.owner+" Balance: "+this.balance;

    }
    public int compareTo(Object o) {
        BankAccount p = (BankAccount) o;
        if(balance>p.balance) return 1;
        if(balance==p.balance) return 0;
        return -1;
    }
}
