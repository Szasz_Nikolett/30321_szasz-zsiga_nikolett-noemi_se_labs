package szasz_zsiga_nikolett.lab6.ex4;

import szasz_zsiga_nikolett.lab6.ex1.BankAccount;

public class Word  {
    private String name;
    public Word(String name)
    {
        this.name=name;
    }
    public String toString()
    {
        return this.name;
    }
    public boolean equals(Object o){
        if(!(o instanceof Word))
            return false;
        Word x = (Word) o;
        return x.name.equals(name);
    }
    public int hashCode(){
        return  name.hashCode();
    }
}
