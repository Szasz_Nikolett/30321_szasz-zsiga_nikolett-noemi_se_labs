package szasz_zsiga_nikolett.lab6.ex4;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Scanner;

public class ConsoleMenu {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Dictionary dictionary = new Dictionary();
        char opt;
        String line, explication;

        do {
            System.out.println("Menu");
            System.out.println("a - Add Word");
            System.out.println("w -list all words ");
            System.out.println("d - list all definition");
            System.out.println("g - get definition");
            System.out.println("e - Exit");

            line = in.nextLine();
            opt = line.charAt(0);

            switch (opt) {
                case 'a':
                case 'A': {
                    System.out.println("Introduce the word:");
                    line = in.nextLine();
                    Word word = new Word(line);
                    Definition definition=null;
                    if (line.length() > 1) {
                        System.out.println("Introduce the definition:");
                        explication = in.nextLine();
                        definition = new Definition(explication);


                    }
                    dictionary.addWord(word, definition);
                }
                break;
                case 'w':
                case 'W': {
                    System.out.println("The list of the words: \n");
                    dictionary.getAllWords();
                }
                break;
                case 'd':
                case 'D': {
                    System.out.println("The list of all definitions:\n");
                    dictionary.getAllDefinitions();
                }
                break;
                case 'g':
                case 'G': {
                    System.out.println("Please introduce the word for which definition you are looking for");
                    line = in.nextLine();
                    Word word = new Word(line);
                    Definition definition = dictionary.getDefinition(word);
                    System.out.println("Final result: " + word + " " + definition);

                }

            }
        } while (opt != 'e' && opt != 'E');
        System.out.println("Program finished.");
    }
}
