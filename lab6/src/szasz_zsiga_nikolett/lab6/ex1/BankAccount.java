package szasz_zsiga_nikolett.lab6.ex1;

public class BankAccount {
    private String owner;
    private double balance;
    public boolean equals(Object o){
        if(o==null||!(o instanceof BankAccount) )
            return false;
        BankAccount x = (BankAccount) o;
        return x.owner.equals(owner)&&x.balance==balance;
    }

    public int hashCode(){
        return (int)balance + owner.hashCode();
    }
    public BankAccount(String owner,double balance)
    {
        this.owner=owner;
        this.balance=balance;
    }
    public void withdraw(double amount)
    {
        System.out.println("Your balance before withdrawal : "+this.balance);
        this.balance=this.balance-amount;
        System.out.println("Your balance after withdrawal : "+this.balance);
    }
    public void deposit (double amount)
    {   System.out.println("Your balance before depositing: "+this.balance);
        this.balance=this.balance+amount;
        System.out.println("Your balance after depositing: "+this.balance);
    }

}
