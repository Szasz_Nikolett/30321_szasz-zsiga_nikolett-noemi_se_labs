package szasz_zsiga_nikolett.lab6.ex1;

public class BankAccountTest {
    public static void main(String[] args) {
        BankAccount bankAccount1 = new BankAccount("Franklin", 10034.23);
        BankAccount bankAccount2 = new BankAccount("Karen", 2550.10);
        BankAccount bankAccount3 = new BankAccount("John", 2550.10);
        BankAccount bankAccount4 = new BankAccount("Franklin", 10034.23);

        if (bankAccount1.equals(bankAccount2))
            System.out.println(bankAccount1+" and "+bankAccount2+" are equals");
        else
            System.out.println(bankAccount1+" and "+bankAccount2+" are not equals");

        if (bankAccount2.equals(bankAccount3))
            System.out.println(bankAccount2+" and "+bankAccount3+" are equals");
        else
            System.out.println(bankAccount2+" and "+bankAccount3+" are not equals");

        if (bankAccount1.equals(bankAccount4))
            System.out.println(bankAccount1+" and "+bankAccount4+" are equals");
        else
            System.out.println(bankAccount1+" and "+bankAccount4+" are not equals");
    }
}
