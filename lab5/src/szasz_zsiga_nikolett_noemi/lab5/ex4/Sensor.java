package szasz_zsiga_nikolett_noemi.lab5.ex4;

public abstract class Sensor {
    private String location;

    abstract int readValue();
    public String getLocation()
    {
        return location;
    }
}
