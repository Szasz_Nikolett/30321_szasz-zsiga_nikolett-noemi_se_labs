package szasz_zsiga_nikolett_noemi.lab5.ex2;

public class Test {
    public static void main(String[] args) {
        Image[] testImage = new Image[4];
        int i = 0;
        testImage[i++] = new RealImage("File1");
        testImage[i++] = new RotatedImage("File2");
        testImage[i++] = new ProxyImage("File3", true);
        testImage[i++] = new ProxyImage("File4", false);

        for (i = 0; i < testImage.length; i++) {
            testImage[i].display();
        }
    }
}
