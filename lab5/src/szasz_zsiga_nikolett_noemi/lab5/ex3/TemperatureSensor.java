package szasz_zsiga_nikolett_noemi.lab5.ex3;
import java.util.Random;
public class TemperatureSensor extends Sensor{
    @Override
    public int readValue() {
        Random r=new Random();
        return r.nextInt((100-0)+1)+0;
    }
}
