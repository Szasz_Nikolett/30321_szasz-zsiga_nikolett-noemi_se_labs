package szasz_zsiga_nikolett_noemi.lab5.ex3;

public class Controller  {
    private Sensor sensor[][];

    public void control()
    {
        Sensor[][] sensors=new Sensor[2][20];
        for(int i=0;i<20;i++)
        {
            sensors[0][i]=new TemperatureSensor();
        }
        for (int i=0;i<20;i++)
        {
            sensors[1][i]=new LightSensor();
        }
        for(int i=0;i<20;i++)
            System.out.println("In the "+(i+1)+" sec the temperature:"+ sensors[0][i].readValue()+" and the light intensity: "+ sensors[1][i].readValue());

    }
}
