package szasz_zsiga_nikolett_noemi.lab5.ex1;

public class Test {
    public static void main(String[] args) {
        Shape[] testShape = new Shape[8];
        int i = 0;
        testShape[i++]=new Circle();
        testShape[i++]=new Rectangle();
        testShape[i++]=new Square();
        testShape[i++]=new Circle(2.5);
        testShape[i++]=new Circle(1.5,"black",false);
        testShape[i++]=new Rectangle(1.5,3.0);
        testShape[i++]=new Rectangle(2.5,4.0,true);
        testShape[i++]=new Square(1.5,"gray",false);
        for(i=0;i<testShape.length;i++)
        {
            System.out.println(testShape[i].getArea()+" "+testShape[i].getPerimeter());
        }
    }
}
