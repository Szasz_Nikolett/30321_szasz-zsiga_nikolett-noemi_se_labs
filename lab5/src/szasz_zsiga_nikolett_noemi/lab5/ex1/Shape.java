package szasz_zsiga_nikolett_noemi.lab5.ex1;

public abstract class Shape {
    protected String color;
    protected boolean filled;

    public Shape()
    {
        color="green";
        filled=true;
    }

    public Shape( String color,boolean filled)
    {
        this.color=color;
        this.filled=filled;
    }
    public String getColor( )
    {
        return color;
    }
    public void setColor(String color)
    {
        this.color=color;

    }
    public boolean isFILLED()
    {
        return filled;
    }
    public void setFILLED(boolean filled)
    {
        this.filled=filled;

    }
    abstract double getArea();
    abstract double getPerimeter();
    public String toString()
    {
        return "A shape with color of "+this.color+" and "+filled;
    }
}
